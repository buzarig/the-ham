// Our Service Section

serviceButtonsArray.forEach((button, buttonIndex) => {
  button.addEventListener("click", () => {
    serviceContentArray.forEach((content, contentIndex) => {
      serviceTrianglesArray.forEach((triangle, triangleIndex) => {
        if (buttonIndex === contentIndex && buttonIndex === triangleIndex) {
          hideServiceContent();
          button.classList.add("service__button-active");
          triangle.classList.remove("service__image-none");
          content.classList.remove("service__content-none");
        }
      });
    });
  });
});

// Our Amazing Work Section

menuButtonsArray.forEach((menuButton, index) => {
  menuButton.addEventListener("click", () => {
    if (index === 0) {
      clearMenuTemplate();
      menuButton.classList.add("menu__button-active");
      getAllImages();
    } else if (index === 1 || index === 2 || index === 3 || index === 4) {
      loadButtons[0].style.display = "none";
      clearMenuTemplate();
      menuButton.classList.add("menu__button-active");
      getImages(index);
    }
  });
});

// Breaking News Section

newsImages.forEach((element) => {
  createNews(element);
});

// What People Say About Ham Section

let userCounter = 0;
getUserInfo(userCounter);

userIconsArray.forEach((user, index) => {
  user.addEventListener("click", () => {
    clearUserInfo();
    getUserInfo(index);
  });
});

rightArrow.addEventListener("click", () => {
  userCounter++;
  if (userCounter === userIconsArray.length) {
    userCounter = 0;
  }
  clearUserInfo();
  getUserInfo(userCounter);
});

leftArrow.addEventListener("click", () => {
  userCounter--;
  if (userCounter < 0) {
    userCounter = userIconsArray.length - 1;
  }
  clearUserInfo();
  getUserInfo(userCounter);
});

// Gallery Of Best Images Section

galleryArray.forEach((galleryImage) => {
  loadButtons[1].addEventListener("click", (evt) => {
    evt.target.classList.add("load");
    setTimeout(() => {
      let elems = [];
      let fragment = document.createDocumentFragment();
      let img = document.createElement("img");
      let imgContainer = document.createElement("div");

      img.src = galleryImage;
      imgContainer.classList.add("gallery-item");
      imgContainer.append(img);
      fragment.append(imgContainer);
      elems.push(imgContainer);
      evt.target.classList.remove("load");
      galleryTemplate.append(fragment);
      msnry.appended(elems);
    }, 2000);
  });
});

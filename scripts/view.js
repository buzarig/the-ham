"use strict";

// Our Services Section

const serviceButtons = document.querySelectorAll(".service__button");
const serviceTriangles = document.querySelectorAll(".service__image");
const serviceContent = document.querySelectorAll(".service__content");

const serviceButtonsArray = [...serviceButtons];
const serviceTrianglesArray = [...serviceTriangles];
const serviceContentArray = [...serviceContent];

function hideServiceContent() {
  serviceButtonsArray.forEach((button) => {
    serviceContentArray.forEach((content) => {
      serviceTrianglesArray.forEach((triangle) => {
        button.classList.remove("service__button-active");
        triangle.classList.add("service__image-none");
        content.classList.add("service__content-none");
      });
    });
  });
}

// Our Amazing Work Section

const menuButtons = document.querySelectorAll(".menu__button");
const menuTemplate = document.querySelector(".menu__template");
const loadButtons = document.querySelectorAll(".load__button");

const menuButtonsArray = [...menuButtons];

function clearMenuTemplate() {
  menuButtonsArray.forEach((element) => {
    element.classList.remove("menu__button-active");
    menuTemplate.innerHTML = "";
    menuTemplate.style.display = "none";
  });
}

const graphicDesign = [
  "./assets/images/graphicdesign/graphic-design1.jpg",
  "./assets/images/graphicdesign/graphic-design2.jpg",
  "./assets/images/graphicdesign/graphic-design3.jpg",
  "./assets/images/graphicdesign/graphic-design4.jpg",
  "./assets/images/graphicdesign/graphic-design5.jpg",
  "./assets/images/graphicdesign/graphic-design6.jpg",
  "./assets/images/graphicdesign/graphic-design7.jpg",
  "./assets/images/graphicdesign/graphic-design8.jpg",
  "./assets/images/graphicdesign/graphic-design9.jpg",
  "./assets/images/graphicdesign/graphic-design10.jpg",
  "./assets/images/graphicdesign/graphic-design11.jpg",
  "./assets/images/graphicdesign/graphic-design12.jpg",
];

const webDesign = [
  "./assets/images/webdesign/web-design1.jpg",
  "./assets/images/webdesign/web-design2.jpg",
  "./assets/images/webdesign/web-design3.jpg",
  "./assets/images/webdesign/web-design4.jpg",
  "./assets/images/webdesign/web-design5.jpg",
  "./assets/images/webdesign/web-design6.jpg",
  "./assets/images/webdesign/web-design7.jpg",
];

const landingPages = [
  "./assets/images/landingpage/landing-page1.jpg",
  "./assets/images/landingpage/landing-page2.jpg",
  "./assets/images/landingpage/landing-page3.jpg",
  "./assets/images/landingpage/landing-page4.jpg",
  "./assets/images/landingpage/landing-page5.jpg",
  "./assets/images/landingpage/landing-page6.jpg",
  "./assets/images/landingpage/landing-page7.jpg",
  "./assets/images/webdesign/web-design1.jpg",
  "./assets/images/webdesign/web-design2.jpg",
  "./assets/images/webdesign/web-design3.jpg",
  "./assets/images/webdesign/web-design4.jpg",
  "./assets/images/webdesign/web-design5.jpg",
];

const wordPress = [
  "./assets/images/wordpress/wordpress1.jpg",
  "./assets/images/wordpress/wordpress2.jpg",
  "./assets/images/wordpress/wordpress3.jpg",
  "./assets/images/wordpress/wordpress4.jpg",
  "./assets/images/wordpress/wordpress5.jpg",
  "./assets/images/wordpress/wordpress6.jpg",
  "./assets/images/wordpress/wordpress7.jpg",
  "./assets/images/wordpress/wordpress8.jpg",
  "./assets/images/wordpress/wordpress9.jpg",
  "./assets/images/wordpress/wordpress10.jpg",
];

const allImages = landingPages;
const allImagesLoader = webDesign.concat(graphicDesign, wordPress);

const menuContent = [
  allImages,
  graphicDesign,
  webDesign,
  landingPages,
  wordPress,
];

loadButtons[0].style.display = "none";

let imgCounter = 0;

function getImages(index) {
  menuContent[index].forEach((element) => {
    let imgWrapper = document.createElement("div");
    let imgHover = document.createElement("div");
    let iconWrapper = document.createElement("div");
    let icon = document.createElement("img");
    let hoverTitle = document.createElement("a");
    let hoverText = document.createElement("a");
    let img = document.createElement("img");
    menuTemplate.style.display = "grid";
    imgWrapper.classList.add("menu__image-wrapper");
    imgHover.classList.add("menu__image-hover");
    icon.src = "./assets/icons/hovericon.png";
    iconWrapper.append(icon);
    hoverTitle.classList.add("menu__image-title");
    hoverTitle.innerText = "creative design";
    hoverText.classList.add("menu__image-text");
    hoverText.innerText = "Web Design";
    imgHover.append(iconWrapper, hoverTitle, hoverText);
    img.src = element;
    imgWrapper.append(imgHover, img);
    menuTemplate.append(imgWrapper);
  });
}

function getAllImages() {
  loadButtons[0].style.display = "flex";
  allImages.forEach((element, index) => {
    let imgWrapper = document.createElement("div");
    let imgHover = document.createElement("div");
    let iconWrapper = document.createElement("div");
    let icon = document.createElement("img");
    let hoverTitle = document.createElement("a");
    let hoverText = document.createElement("a");
    let img = document.createElement("img");
    menuTemplate.style.display = "grid";
    imgWrapper.classList.add("menu__image-wrapper");
    imgHover.classList.add("menu__image-hover");
    icon.src = "./assets/icons/hovericon.png";
    iconWrapper.append(icon);
    hoverTitle.classList.add("menu__image-title");
    hoverTitle.innerText = "creative design";
    hoverText.classList.add("menu__image-text");
    hoverText.innerText = "Web Design";
    imgHover.append(iconWrapper, hoverTitle, hoverText);
    if (index <= 11) {
      img.src = allImages[index];
      imgWrapper.append(imgHover, img);
      menuTemplate.append(imgWrapper);
    }
  });
}
getAllImages();

loadButtons[0].addEventListener("click", () => {
  loadButtons[0].classList.add("load");
  setTimeout(() => {
    for (let i = 0; i < 12; i++) {
      let imgWrapper = document.createElement("div");
      let imgHover = document.createElement("div");
      let iconWrapper = document.createElement("div");
      let icon = document.createElement("img");
      let hoverTitle = document.createElement("a");
      let hoverText = document.createElement("a");
      let img = document.createElement("img");
      imgWrapper.classList.add("menu__image-wrapper");
      imgHover.classList.add("menu__image-hover");
      icon.src = "./assets/icons/hovericon.png";
      iconWrapper.append(icon);
      hoverTitle.classList.add("menu__image-title");
      hoverTitle.innerText = "creative design";
      hoverText.classList.add("menu__image-text");
      hoverText.innerText = "Web Design";
      imgHover.append(iconWrapper, hoverTitle, hoverText);

      img.src = allImagesLoader[imgCounter];
      imgWrapper.append(imgHover, img);
      menuTemplate.append(imgWrapper);
      imgCounter++;
      if (imgCounter === 24) {
        imgCounter = 0;
        loadButtons[0].style.display = "none";
      }
    }
    loadButtons[0].classList.remove("load");
  }, 2000);
});

// Breaking News Section

const newsTemplate = document.querySelector(".news__contents");
const news = document.querySelectorAll(".news__content");
const newsImages = [
  "./assets/images/breakingNews/news1.png",
  "./assets/images/breakingNews/news2.png",
  "./assets/images/breakingNews/news3.png",
  "./assets/images/breakingNews/news4.png",
  "./assets/images/breakingNews/news5.png",
  "./assets/images/breakingNews/news6.png",
  "./assets/images/breakingNews/news7.png",
  "./assets/images/breakingNews/news8.png",
];

function createNews(element) {
  let newsContainer = document.createElement("div");
  newsContainer.classList.add("news__content");
  let contentItem = document.createElement("div");
  contentItem.classList.add("content-item");
  let dateContainer = document.createElement("div");
  dateContainer.classList.add("content-date");
  let date = document.createElement("span");
  let month = document.createElement("span");
  date.innerHTML = "12";
  month.innerHTML = "Feb";
  dateContainer.append(date, month);
  contentItem.append(dateContainer);
  let img = document.createElement("img");
  img.src = element;
  let infoTitle = document.createElement("a");
  infoTitle.classList.add("content-title");
  infoTitle.innerHTML = "Amazing Blog Post";
  infoTitle.href = "#";

  let itemInfo = document.createElement("div");
  itemInfo.classList.add("item-info");
  let infoUser = document.createElement("span");
  infoUser.classList.add("info-user");
  infoUser.innerHTML = "By admin |";
  let infoComments = document.createElement("span");
  infoComments.classList.add("info-comments");
  infoComments.innerHTML = " 2 comment";

  contentItem.append(img);
  itemInfo.append(infoUser, infoComments);
  newsContainer.append(contentItem, infoTitle, itemInfo);
  newsTemplate.append(newsContainer);
}

// What People Say About Ham Section

let userText = document.querySelector(".user_text");
let userName = document.querySelector(".user_name");
let userJob = document.querySelector(".user_job");
let userPicture = document.querySelector(".user__image-wrapper");
let userIcons = document.querySelectorAll(".user-icon");

const leftArrow = document.querySelector(".left_arrow");
const rightArrow = document.querySelector(".right_arrow");

const userIconsArray = [...userIcons];

const texts = [
  "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias cumque, perferendis ullam facere totam sapiente deserunt et repellendus est voluptatem numquam, quod a voluptate doloribus ipsa eaque aliquid quae at quisquam officia dolor libero.",
  "Molestias cumque, perferendis ullam facere totam sapiente deserunt et repellendus est voluptatem numquam, quod a voluptate doloribus ipsa eaque aliquid quae at quisquam officia dolor libero.",
  "Eveniet nesciunt voluptatum odio laborum nam ipsam alias cupiditate totam aliquam, repellendus quidem at veritatis? Voluptas, enim quam. Enim aliquid a vel eum! Eius, magni? Animi, quaerat ducimus soluta quam quia debitis. Doloribus voluptatem, quas suscipit nisi.",
];

const names = ["Butcher", "Pop Smoke", "Joe Biden", "Invoker"];

const jobs = [
  "UI Designer",
  "Python Senior Developer",
  "Software Engineer",
  "C++ Developer",
];

const pictures = [
  "./assets/icons/user-1.png",
  "./assets/icons/user-2.png",
  "./assets/icons/user-3.png",
  "./assets/icons/user-4.png",
];

let userImg = document.createElement("img");
function getUserInfo(index) {
  userImg.classList.add("user__image");
  userImg.classList.add("image-animation");
  userPicture.append(userImg);
  userText.innerHTML = texts[index];
  userName.innerHTML = names[index];
  userJob.innerHTML = jobs[index];
  userImg.src = pictures[index];
  userIconsArray[index].classList.add("user-choosen");
}

function clearUserInfo() {
  userIconsArray.forEach((element) => {
    userText.innerHTML = "";
    userName.innerHTML = "";
    userJob.innerHTML = "";
    userImg.src = "";
    element.classList.remove("user-choosen");
  });
}

// Gallery Of Best Images Section

let gallery = document.querySelector(".gallery__template");

const galleryTemplate = document.querySelector(".gallery__template");
let msnry;
window.onload = function () {
  msnry = new Masonry(galleryTemplate, {
    columnWidth: 373,
    itemSelector: ".gallery-item",
    gutter: 20,
  });
};

const galleryArray = [
  "./assets/images/gallery/gallery-item (2).JPG",
  "./assets/images/gallery/gallery-item (3).JPG",
  "./assets/images/gallery/gallery-item (8).JPG",
  "./assets/images/gallery/gallery-item (4).JPG",
  "./assets/images/gallery/gallery-item (5).JPG",
  "./assets/images/gallery/gallery-item (7).JPG",
  "./assets/images/gallery/gallery-item (1).JPG",
  "./assets/images/gallery/gallery-item (9).JPG",
  "./assets/images/gallery/gallery-item (6).JPG",
  "./assets/images/gallery/gallery-item (10).JPG",
];

function getGalleryImages() {
  galleryArray.forEach((element, index) => {
    let img = document.createElement("img");
    let imgContainer = document.createElement("div");
    if (index <= 10) {
      img.src = galleryArray[index];
      imgContainer.classList.add("gallery-item");
      imgContainer.append(img);
      gallery.append(imgContainer);
    }
  });
}
getGalleryImages();

galleryArray.forEach((galleryImage) => {
  loadButtons[1].addEventListener("click", () => {
    loadButtons[1].classList.add("load");
    setTimeout(() => {
      let elems = [];
      let fragment = document.createDocumentFragment();
      let img = document.createElement("img");
      let imgContainer = document.createElement("div");

      img.src = galleryImage;
      imgContainer.classList.add("gallery-item");
      imgContainer.append(img);
      fragment.append(imgContainer);
      elems.push(imgContainer);
      loadButtons[1].classList.remove("load");
      galleryTemplate.append(fragment);
      msnry.appended(elems);
    }, 2000);
  });
});
